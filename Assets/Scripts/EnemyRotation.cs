using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRotation : MonoBehaviour
{
    public int Direction;
    // Start is called before the first frame update
    void Start()
    {
        Direction = +1;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.localRotation.eulerAngles.y<=1)
        {
            Debug.Log("1�Giro");
            transform.Rotate(new Vector3(0, 20f, 0) * Time.deltaTime);
        }
        if (transform.localRotation.eulerAngles.y >= -180)
        {
            Debug.Log("2�Giro");
            transform.Rotate(new Vector3(0, -20f, 0) * Time.deltaTime);
        }

    }
}

