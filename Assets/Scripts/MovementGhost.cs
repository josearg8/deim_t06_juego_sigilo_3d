using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MovementGhost : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public Vector3 Destiny;
    public Transform Position;

    public List<Transform> Destinys;

    private int currentDestinationIndex;

    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent.SetDestination(Destinys[0].position);
        currentDestinationIndex = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(navMeshAgent.remainingDistance<=0.01f)
        {
            currentDestinationIndex += 1;
            if (currentDestinationIndex>= Destinys.Count)
            {
                currentDestinationIndex = 0;
            }
            navMeshAgent.SetDestination(Destinys[currentDestinationIndex].position);
            
        }
    }
}
